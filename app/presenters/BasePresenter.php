<?php

namespace App\Presenters;

use Nette;
use App\Model;
use App\Model\Helpers\VozidloHelper;
use App\Model\Helpers\BookHelper;
use App\Model\Helpers\UzivatelHelper;
use App\Model\Helpers\VydajeHelper;



/**
 * Base presenter for all application presenters.
 */
abstract class BasePresenter extends Nette\Application\UI\Presenter
{

	/** @var VozidloHelper @inject*/
	public $vozidloHelper;

	/** @var BookHelper @inject */
	public $bookHelper;

	/** @var UzivatelHelper @inject */
	public $uzivatelHelper;

	/** @var VydajeHelper @inject */
	public $vydajeHelper;

	const ADMIN=0;
	const UZIVATEL=1;

	public function startup()
	{
		if(!$this->user->isLoggedIn()){
			$backlink = $this->storeRequest();
			$this->forward('Sign:in', array('backlink' => $backlink));
		}
		parent::startup();
	}
}
