<?php

namespace App\Presenters;

use Nette;
use App\Forms\SignFormFactory;


class SignPresenter extends Nette\Application\UI\Presenter
{
	/** @var SignFormFactory @inject */
	public $factory;

	/** @persistent */
	public $backlink = null;


	public function beforeRender(){
		if($this->user->isLoggedIn()){
			$this->redirect("Homepage:default");
		}
	}


	/**
	 * Sign-in form factory.
	 * @return Nette\Application\UI\Form
	 */
	protected function createComponentSignInForm()
	{
		$form = $this->factory->create();
		$form->onSuccess[] = $this->redir;
		return $form;
	}


	public function actionOut()
	{
		$this->getUser()->logout();
		$this->flashMessage('Odhlášení bylo úspěšné.', 'alert-warning');
		$this->redirect('in');
	}


	public function redir(){
		if($this->backlink != null){
			$this->restoreRequest($this->backlink);
		}
		$this->redirect('Homepage:default');
	}

}
