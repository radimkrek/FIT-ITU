<?php

namespace App\Model\Entities;


class Uzivatel extends BaseEntity
{
	/** @var int */
	protected $id;

	/** @var string */
	protected $username;

	/** @var string */
	protected $password;

	/** @var string */
	protected $jmeno;

	/** @var string */
	protected $prijmeni;

	/** @var string */
	protected $role;

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 * @return Uzivatel
	 */
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getUsername()
	{
		return $this->username;
	}

	/**
	 * @param string $username
	 * @return Uzivatel
	 */
	public function setUsername($username)
	{
		$this->username = $username;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getPassword()
	{
		return $this->password;
	}

	/**
	 * @param string $password
	 * @return Uzivatel
	 */
	public function setPassword($password)
	{
		$this->password = $password;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getPrijmeni()
	{
		return $this->prijmeni;
	}

	/**
	 * @param string $prijmeni
	 * @return Uzivatel
	 */
	public function setPrijmeni($prijmeni)
	{
		$this->prijmeni = $prijmeni;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getJmeno()
	{
		return $this->jmeno;
	}

	/**
	 * @param string $jmeno
	 * @return Uzivatel
	 */
	public function setJmeno($jmeno)
	{
		$this->jmeno = $jmeno;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getRole()
	{
		return $this->role;
	}

	/**
	 * @param string $role
	 * @return Uzivatel
	 */
	public function setRole($role)
	{
		$this->role = $role;
		return $this;
	}

	public function toArray(array $notIncluded = array()){
		return parent::toArray($notIncluded);
	}
}