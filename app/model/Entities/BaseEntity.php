<?php


namespace App\Model\Entities;


use Nette\Database\Table\ActiveRow;
use Nette\Utils\ArrayHash;

class BaseEntity
{
	public function __construct($data = null) {
		if(!is_null($data)){
			$this->set($data);
		}
	}
	/**
	 * Nastav� vlastnosti objektu.
	 * Proch�z� p�edan� data a vyhled� odpov�daj�c� metody = settery objektu, pomoc� nich� nastav� vlastnosti objektu
	 * Metoda mus� m�t n�zev set[vlastnost|index] jako valstnost|index v poli/objektu $data
	 * @param \DibiRow|\Nette\ArrayHash|array|\Nette\Database\Table\ActiveRow|null $data Data p�edan� objektu
	 */
	public function set($data = null) {
		if (!is_null($data)) {
			if ($data instanceof \DibiRow or $data instanceof ArrayHash) {
				$vars = get_object_vars($data);
				foreach ($vars as $key => $value) {
					$method = 'set' . ucfirst($key);
					if (method_exists($this, $method)) {
						$this->$method($value);
					}
				}
			} elseif (is_array($data)) {
				foreach ($data as $key => $value) {
					$method = 'set' . ucfirst($key);
					if (method_exists($this, $method)) {
						$this->$method($value);
					}
				}
			} elseif ($data instanceof ActiveRow) {
				$data = $data->toArray();
				foreach ($data as $key => $value) {
					$method = 'set' . ucfirst($key);
					if (method_exists($this, $method)) {
						$this->$method($value);
					}
				}
			}
		}
	}

	/**
	 * Vrac� objekt jako pole
	 * @param array $notIncluded pole properties, kter� se nemaj� vracet
	 * @return \Nette\ArrayHash pole vlastnost� objektu
	 */
	public function toArray(array $notIncluded = array()) {
		$notIncluded[] = 'activeRow';
		$vars = get_object_vars($this);
		$ret = array();
		foreach ($vars as $key => $value) {
			if (!in_array($key, $notIncluded)) {
				if ($value instanceof BaseEntity) {
					$ret[$key] = $value->toArray($notIncluded);
				}
				else {
					$ret[$key] = $value;
				}
			}
		}
		return ArrayHash::from($ret);
	}
}