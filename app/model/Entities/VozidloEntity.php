<?php

namespace App\Model\Entities;


class Vozidlo extends BaseEntity
{
	/** @var int */
	protected $id;

	/** @var string */
	protected $nazev;

	/** @var string */
	protected $spz;

	/** @var string */
	protected $tachometr;

	/** @var \DateTime */
	protected $stk;

	/**
	 * @return \DateTime
	 */
	public function getStk()
	{
		return $this->stk;
	}

	/**
	 * @param \DateTime $stk
	 * @return Vozidlo
	 */
	public function setStk($stk)
	{
		$this->stk = $stk;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getTachometr()
	{
		return $this->tachometr;
	}

	/**
	 * @param mixed $tachometr
	 * @return Vozidlo
	 */
	public function setTachometr($tachometr)
	{
		$this->tachometr = $tachometr;
		return $this;
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 * @return Vozidlo
	 */
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getNazev()
	{
		return $this->nazev;
	}

	/**
	 * @param string $nazev
	 * @return Vozidlo
	 */
	public function setNazev($nazev)
	{
		$this->nazev = $nazev;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getSpz()
	{
		return $this->spz;
	}

	/**
	 * @param string $spz
	 * @return Vozidlo
	 */
	public function setSpz($spz)
	{
		$this->spz = $spz;
		return $this;
	}

	public function toArray(array $notIncluded = array()){
		return parent::toArray($notIncluded);
	}
}