<?php


namespace App\Model\Helpers;

use Nette\Database\Context;


class VozidloHelper
{
	/** @var Context */
	private static $database;

	private static $table = 'vozidlo';

	public function __construct(Context $database) {
		self::$database = $database;
	}

	public function getAll($order = 'ASC', $limit = 2147483647, $offset = 0){
		$raw = self::$database->table(self::$table)->select('*')->order('id ' . $order)->limit($limit, $offset);

		$data = $raw->fetchAll();

		$ret = array();
		foreach($data as $d){
			$ret[] = new \App\Model\Entities\Vozidlo($d);
		}
		return $ret;
	}

	public function search($query){
		$data = self::$database->table(self::$table)->select('*')->where('LOWER(nazev) LIKE LOWER(?)', '%'.$query.'%')->fetchAll();
		$ret = array();
		foreach($data as $d){
			$ret[] = new \App\Model\Entities\Vozidlo($d);
		}
		return $ret;
	}

	public function getById($id){
		$data = self::$database->table(self::$table)->select('*')->where('id', $id)->fetch();
		return new \App\Model\Entities\Vozidlo($data);
	}

	public function save(\App\Model\Entities\Vozidlo $data){
		if($data->getId() != null){ //editace
			$ret = self::$database->table(self::$table)->where('id', $data->getId())->update($data->toArray());
		}else{ //vlozeni noveho zaznamu
			$ret = self::$database->table(self::$table)->insert($data->toArray());
		}

		return new \App\Model\Entities\Vozidlo($ret);
	}

	public function delete($id){
		self::$database->table(self::$table)->where('id', $id)->delete();
	}

	public function count(){
		return count(self::$database->table(self::$table)->select('id')->fetchAll());
	}
}